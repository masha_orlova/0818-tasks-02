#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 25

int lensq(int arr[])
{
	int length, temp, i = 0, maxlen = 0;
	 while(i < N){
		temp = arr[i];
		length = 1;
		i++;
		while((i < N)&&(temp == arr[i])){
			i++;
			length++;
		}
		if(length > maxlen)
			maxlen = length;			
	}
	return maxlen;
}

int main()
{
	int array[N],i;
	srand(time(0));
	for(i = 0; i < N; i++){
		array[i] = rand()%(N / 6) + 1; 
		printf("%d ", array[i]);
	}
	printf("\n%d\n", lensq(array));
	return 0;
}