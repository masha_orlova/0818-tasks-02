#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

#define	n 25

int maxs(int (*p)[n])
{
	int i,j,col=0,max=0,sum=0;
	for (j=0; j<n; j++)
	{
		for (i=0; i<n; i++)
			sum+=p[i][j];
		if (max<sum)
		{
			max=sum;
			col=j;
		}
		sum=0;
	}
	return col;
}

int main()
{
	int matrix[n][n];
	int i,j;
	srand(time(0));
	for (i=0; i<n; i++)
		for (j=0; j<n; j++)
			matrix[i][j]=rand()%13;
	printf("Enter your matrix:\n");
	for (i=0; i<n; i++)
	{
		for (j=0; j<n; j++)
		 printf("%5d",matrix[i][j]);
		 printf("\n");
	}
	printf("The column number: %d", maxs(matrix)+1);
	getch();
	return 0;
}