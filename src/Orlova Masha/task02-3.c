#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 25

void farray(int arr[][N], int numf, int numl) 
{
	int i, j;
	srand(time(0));
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < N; j++)
		{
			arr[i][j] = numf + rand() % (numl - numf + 1);
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

int comp(const void *i, const void *j) 
{
  return *(int *)i - *(int *)j;
}


int lengthsq(int arr[][N])
{
	int i, j, count, max, str;
	count = 1;
	max = 0;
	for(i = 0; i < N; i++)
	{
		for(j = 1; j < N+1; j++)
		{
			if((arr[i][j] == arr[i][j-1]) && (j != N))
			{
				count++;
			}
			else if(count > max)
			{
				max = count;
				count = 1;
				str = i+1;
			}
			else
			{		
			count = 1;
			}
		}
	}
	return str;
}

int main()
{
	int array[N][N], i, j, numf, numl;

	printf("Enter a range of numbers:\n");
	scanf("%d %d", &numf, &numl);

	farray(array, numf, numl);

	for(i = 0; i < N; i++)
	{
		qsort(array[i], N, sizeof(int), comp);
	}
	
	for(i = 0; i < N; i++)
        {
                for(j = 0; j < N; j++)
                {            
                	printf("%d ", array[i][j]);
                }
                printf("\n");
        }

	printf("Max string is %d\n", lengthsq(array));
	return 0;
}