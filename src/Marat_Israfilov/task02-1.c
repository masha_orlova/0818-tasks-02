/*Программа, определяющая самую длинную последовательность повторяющихся чисел и выводящая её длину */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 25

void fill_array(int* arr, int num1, int num2) //заполняем массив случайными числами от num1 до num2
{
	int i;
	srand(time(0));
	for(i = 0; i < N; i++)
	{
		arr[i] = num1 + rand() % (num2 - num1 + 1);
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int sequence_length(int* arr) //сравниваем длинны последовательностей
{
	int i, count, max;
	count = 1;
	max = 0;
	for(i = 1; i < N+1; i++)
	{
		if((arr[i] == arr[i-1]) && (i != N))
		{
			count++;
		}
		else if(count > max)
		{
			max = count;
			count = 1;
		}
		else
		{		
		count = 1;
		}
	}
	return max;
}

int comp(const void *i, const void *j) //сортируем элементы по возрастанию в qsort
{
  return *(int *)i - *(int *)j;
}

int main()
{
	int arr[N], num1, num2, i;
	
	printf("Enter a range of numbers(num1 num2):\n");
	scanf("%d %d", &num1, &num2);	
	fill_array(arr, num1, num2);
	qsort(arr, N, sizeof(int), comp);
	
	for(i = 0; i < N; i++)
	{
		printf("%d ", arr[i]);
	}
	
	printf("\nMax sequence length: %d\n",sequence_length(arr));
	return 0;
}
