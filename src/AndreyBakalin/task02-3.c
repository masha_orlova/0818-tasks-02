#include <stdio.h>
#include <locale.h>

#define N 3

int repeats(int arr[][N], int S)
{
	int i, j, k, max_repeats = 1, cur_repeats, res_line = 0;

	for (i = 0; i < S; i++)
	{
		for (j = 0; j < N; j++)
		{
			cur_repeats = 1;

			for (k = j + 1; k < N; k++)
				if (arr[i][j] == arr[i][k])
					cur_repeats++;

			if (cur_repeats > max_repeats)
			{
				max_repeats = cur_repeats;
				res_line = i;
			}
		}
	}

	return res_line + 1;
}


int main()
{
	setlocale(LC_ALL, "rus");

	int arr[N][N] = { { 1, 1, 2 }, 
					  { 2, 2, 2 }, 
					  { 3, 3, 1 } };
	int i, j;

	printf("�������� ������������� ������ ��������� � ������ ����� %d\n", repeats(arr, N));

	return 0;
}