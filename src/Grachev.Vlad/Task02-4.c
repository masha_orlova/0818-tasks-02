#include <stdio.h>
#include <time.h>
#define N 10

int maxStSum(int (*arr)[N])
{
	int i, j, maxsum = 0;
	for(j = 0; j < N; j++){
		for(i = N - 2; i >= 0; i--)
			arr[N-1][j] = arr[N-1][j] + arr[i][j];
	if(arr[N-1][j] > maxsum)
		maxsum = arr[N-1][j];
	}
	return maxsum;
}

int main()
{
	int array[N][N];
	int i,j;
	srand(time(0));
	for(i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			array[i][j] = rand() % (N / 2) + 1;
			printf("%3d ", array[i][j]);
		}
		printf("\n");
	}
	printf("MaxStSum: %d\n", maxStSum(array));
	return 0;
}