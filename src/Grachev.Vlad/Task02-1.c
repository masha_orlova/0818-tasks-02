#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 20

int lngSq(int arr[])
{
	int lngth, buf, i = 0, maxlngth = 0;
	while(i < N){
		buf = arr[i];
		lngth = 1;
		i++;
		while((i < N)&&(buf == arr[i])){
			i++;
			lngth++;
		}
		if(lngth > maxlngth)
			maxlngth = lngth;			
	}
	return maxlngth;
}

int main()
{
	int array[N],i;
	srand(time(0));
	for(i = 0; i < N; i++){
		array[i] = rand()%(N / 6) + 1; 
		printf("%d ", array[i]);
	}
	printf("\n%d\n", lngSq(array));
	return 0;
}