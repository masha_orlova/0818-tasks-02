#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 5
int Max_Rep(int(*Matr)[N])
{
	char i = 0, j = 0, z = 0, Count = 1, MCount = 0,Res=0;
	for (i = 0;i < N;i++)
		for (j = 0;j < N;j++)
		{
			for (z = j + 1;z < N;z++)
				if (Matr[i][j] == Matr[i][z])
					Count++;
			if (Count > MCount)
			{
				MCount = Count;
				Res = i;
			}
			Count = 1;
		}
	return Res+1;
}
void Demonstration()
{
	srand(time(NULL));
	int Matr[N][N];
	char i, j;
	puts("----DEMONSTRATION BEGINS----\nFunction 'Max_Rep' defining in a square matrix of the size 5 the line having\nthe greatest number of repetitions of some value\nThe matrix filled with random numbers");
	for (i = 0;i < N;i++)
	{
		for (j = 0;j < N;j++)
		{
			Matr[i][j] = 0 + rand() % 6;
			printf("%4d", Matr[i][j]);
		}
		puts("");
	}
	printf("Result: %d\n", Max_Rep(Matr));
	puts("----DEMONSTRATION IS OVER---\n");
}
int main()
{
	Demonstration();
	return 0;
}