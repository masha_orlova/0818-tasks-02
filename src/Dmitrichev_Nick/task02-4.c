#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 5
int Max_Sum_Col(int(*Matr)[N])
{
	char i = 0, j = 0, Res = 0, Sum = 0, Max_Sum = 0;
	for (j = 0;j < N;j++) 
	{
		Sum = 0;
		for (i = 0;i < N;i++)
			Sum += Matr[i][j];
		if (Sum > Max_Sum)
		{
			Max_Sum = Sum;
			Res = j;
		}
	}
	return Res+1;
}
void Demonstration()
{
	srand(time(NULL));
	int Matr[N][N];
	char i, j;
	puts("----DEMONSTRATION BEGINS----\nFunction 'Max_Sum_Col' defining in a square matrix of the size 5 a column\nwith the maximum sum of elements\nThe matrix filled with random numbers");
	for (i = 0;i < N;i++)
	{
		for (j = 0;j < N;j++)
		{
			Matr[i][j] = -5 +rand() % 11;
			printf("%4d", Matr[i][j]);
		}
		puts("");
	}
	printf("Column with the largest sum of elements: %d\n", Max_Sum_Col(Matr) + 1);
	puts("----DEMONSTRATION IS OVER---\n");
}
int main()
{
	Demonstration();
	return 0;
}