#include <stdio.h>
#include <conio.h>
#include <time.h>
#include <stdlib.h>
#define n	20

int conslen(int *arr)
{
	int i=0,count=1,max=1;
	while (i<n)
	{
		if (arr[i] == arr[i+1])
			count++;
		else
		{
			if (max < count)
				max = count;
			count = 1;
		}
		i++;
	}
	return max;
}

int main()
{
	int arr[n],i;
	printf("Your array is:\n");
	for (i=0;i<n;i++)
	{
		arr[i] = rand()%10;
		printf("%d ",arr[i]);
	}
	printf("\n");
	printf("The longest consecutive is %d",conslen(arr));
	getch();
	return 0;
}