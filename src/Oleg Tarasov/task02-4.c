#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>

#define N 15
#define M 10

int GetFieldSum(int arr[][M], int num_rows, int field)
{
	int sum = 0;
	for (int i = 0; i < num_rows; i++)
		sum += arr[i][field];
	return sum;
}

int GetMaxFieldSum(int arr[][M], int num_rows, int * field)
{
	int sum = 0, maxfield = 0;
	for (int i = 0; i < M; i++) {
		int temp = GetFieldSum(arr, N, i);
		if (temp > sum)
		{
			sum = temp;
			maxfield = i;
		}
	}
	*field = maxfield;
	return sum;
}


int main()
{
	srand(time(0));
	setlocale(LC_ALL, "Russian");
	int arr[N][M];
	int field = 0, sum = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++) {
			arr[i][j] = rand() % 15;
			printf("%2d ", arr[i][j]);
		}
		printf("\n");
	}
	sum = GetMaxFieldSum(arr, N, &field);
	printf("������ ������� � ������������ ������ ���������: %d (sum = %d)\n", field, sum);
	return 0;
}