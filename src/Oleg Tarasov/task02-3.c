#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>

#define M 10

void InsertionSort(int * arr, int num)
{
	int key = 0, i;
	for (int j = 1; j < num; j++) {
		key = arr[j];
		i = j - 1;
		while (i >= 0 && arr[i] > key) {
			arr[i + 1] = arr[i];
			i--;
		}
		arr[i + 1] = key;
	}
}

int GetMaxRepeats(int *arr, int num)
{
	int * TempArr = (int*)calloc(num, sizeof(int));
	int val = 0, max = 0;
	for (int i = 0; i < num; i++) {
		TempArr[i] = arr[i];
	}
	InsertionSort(TempArr, num);
	int i = 0;
	while (i < num) {
		int key = TempArr[i], tempNum = 0;
		while (i < num && TempArr[i] == key) {
			tempNum++;
			i++;
		}
		if (tempNum > max) {
			max = tempNum;
			val = key;
		}
	}
	free(TempArr);
	return max;
}

int GetStringWithMaxRepeats(int(*matrix)[M], int N)
{
	int index = -1, lastmax = 0;
	for (int i = 0; i < N; i++) {
		int temp = GetMaxRepeats(matrix[i], M);
		if (temp > lastmax) {
			index = i;
			lastmax = temp;
		}
	}
	return index;
}



int main()
{
	srand(time(0));
	setlocale(LC_ALL, "Russian");
	int arr[M][M];
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < M; j++) {
			arr[i][j] = rand() % 2;
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	printf("������ � ������������ ������ ���������� ��������: %d\n", GetStringWithMaxRepeats(arr, M));
	return 0;
}