#include <stdio.h>

int IsPalindrom(char * arr, int length)
{
	int center = length / 2;
	for (int i = 0; i < center; i++) {
		if (arr[i] != arr[length - 1 - i])
			return 0;
	}
	return 1;
}

int main()
{
	char str1[] = "1232141", str2[] = "123321", str3[] = "1ac2321";
	printf("%s :: PalindromDetect: %d\n", str1, IsPalindrom(str1, sizeof(str1) / sizeof(char) - 1));
	printf("%s :: PalindromDetect: %d\n", str2, IsPalindrom(str2, sizeof(str2) / sizeof(char) - 1));
	printf("%s :: PalindromDetect: %d\n", str3, IsPalindrom(str3, sizeof(str3) / sizeof(char) - 1));
	return 0;
}