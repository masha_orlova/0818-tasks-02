// �������� �������, ������������ � ��������� ������� (�������) ������� � ������������ ������ ���������. ������ ���������� � �������� ���������. �������� ���������������� ���������.
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>
#define MATRIX_ROWS 4
#define MATRIX_COLUMNS 15

int matrix_maxcol(int matr[][MATRIX_COLUMNS]) {

	int i, j;
	int sum = 0,
		maxsum = 0,
		maxsum_column = 0;

	// ���������� ����� ��������� � ������ �������
	for (i = 0; i < MATRIX_ROWS; i++) {
		maxsum += matr[i][0];
	}

	// ������� ��������� �����
	for (i = 1; i < MATRIX_COLUMNS; i++) {
		for (j = 0; j < MATRIX_ROWS; j++) {
			sum += matr[j][i];
		}

		if (sum > maxsum) {
			maxsum = sum;
			maxsum_column = i;
		}
		sum = 0;
	}

	return maxsum_column;
}

int main() {

	int i, j;
	int matrix[MATRIX_ROWS][MATRIX_COLUMNS];

	srand(time(0));
	setlocale(LC_ALL, "rus");

	for (i = 0; i < MATRIX_ROWS; i++) {
		for (j = 0; j < MATRIX_COLUMNS; j++) {
			printf("%d ", matrix[i][j] = rand() % 10); // ��������� ������� ������� � ������� ��
		}
		puts("");
	}

	printf("������� � ������������ ������ ���������: %d\n", matrix_maxcol(matrix) + 1);

	system("pause");

	return 0;
}