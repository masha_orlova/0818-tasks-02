// �������� �������, ������������ � ��������� ������� (�������) ������, ������� ���������� ���������� ���������� ���������� ��������. ������ ���������� � �������� ���������. �������� ���������������� ���������.
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>
#define ROWS 4
#define COLUMNS 10

int simpleststr(char str[][COLUMNS]) {

	int i, j, k;
	int symbols_counter;
	int maxsymbols_counter; // ���������� ���������� ���������� �������� � ������ ������ 
	int	maxsymbols_allstr = 0; // ���������� ���������� ���������� ����� ���� �����
	int simplestr; // ������ � ���������� ����������� ���������� ��������

	// ���������� �������� ������������� ������ � ������ �� �����
	for (i = 0; i < ROWS; i++) {

		maxsymbols_counter = 0;

		for (j = 0; j < COLUMNS; j++) {
			symbols_counter = 1; // ������� ��� ���������� ������� ������

			for (k = j + 1; k < COLUMNS; k++) {
				if (str[i][k] == str[i][j]) symbols_counter++;
			}

			if (symbols_counter > maxsymbols_counter) {
				maxsymbols_counter = symbols_counter;
			}
		}

		if (maxsymbols_counter > maxsymbols_allstr) {
			maxsymbols_allstr = maxsymbols_counter;
			simplestr = i;
		}
	}

	return simplestr; // ���������� ������ �������, � �� ����� ������!
}

int main() {

	int i, j;
	char str[ROWS][COLUMNS] = {
		{'1', '1', '2', '2', '3', '3', '4', '4', '5', '5'},
		{'1', '1', '1', '2', '2', '2', '3', '3', '3', '4' },
		{'1', '1', '1', '1', '2', '2', '2', '2', '3', '3' },
		{'1', '1', '2', '2', '3', '3', '4', '4', '5', '5' }
	};

	srand(time(0));
	setlocale(LC_ALL, "rus");

	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLUMNS; j++) {
			// ��������� ������ ������, ��������� � �������� ��� ����������� � ������� ��
			// str[i][j] = rand() % 3 + '0';
			printf("%c", str[i][j]);
		}
		puts("");
	}

	// 3
	printf("������, ������� ���������� ���������� ���������� ���������� ��������: %d\n", simpleststr(str)+1);

	system("pause");

	return 0;
}